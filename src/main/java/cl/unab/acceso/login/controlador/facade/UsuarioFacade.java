package cl.unab.acceso.login.controlador.facade;

import cl.unab.acceso.login.controlador.LoginControlador;
import cl.unab.acceso.login.controlador.singleton.DatoEncriptacionSingleton;
import cl.unab.acceso.login.controlador.singleton.EnvioMensajeSingleton;
import cl.unab.acceso.login.modelo.dao.UsuarioDao;
import cl.unab.acceso.login.modelo.dto.UsuarioDto;
import cl.unab.acceso.login.vista.UsuarioVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class UsuarioFacade implements IUsuarioFacade {

    private static Logger log = LoggerFactory.getLogger(LoginControlador.class);

    private UsuarioDao usuarioDao;

    @Override
    public boolean create(UsuarioVo usuarioVo, StringBuffer message) {
        return false;
    }

    @Override
    public boolean update(UsuarioVo usuarioVo, StringBuffer message) {
        return false;
    }

    @Override
    public boolean delete(UsuarioVo usuarioVo, StringBuffer message) {
        return false;
    }

    @Override
    public List<UsuarioVo> listAll(UsuarioVo usuarioVo, StringBuffer message) {
        return null;
    }

    @Override
    public UsuarioVo list(UsuarioVo usuarioVo, StringBuffer message) {
        return null;
    }
}
