package cl.unab.acceso.login.controlador.singleton;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EnvioMensajeSingleton {

    private static Logger log = LoggerFactory.getLogger(EnvioMensajeSingleton.class);

    public static final String ACCOUNT_MESSAGE =
            "Sistema Repositorio Documentos Academicos --> Codigo de Acceso: ";

    public static final String RECOVERY_MESSAGE =
            "Sistema Repositorio Documentos Academicos --> Password de Acceso: ";

    public static final String ACCOUNT_AREA =
            "+56";
    public static final String ACCOUNT_PHONE =
            "+14704227976";
    public static final String ACCOUNT_SID =
            "AC2c1b4fb7f2723eea5455c0eb01a69165";
    public static final String AUTH_TOKEN =
            "a1248b6ddb64e17ddc886f2006fea166";

    private static EnvioMensajeSingleton envioMensajeSingleton;

    public synchronized static EnvioMensajeSingleton getEnvioMensajeSingleton(){
        if(envioMensajeSingleton == null){
            envioMensajeSingleton = new EnvioMensajeSingleton();
        }
        return envioMensajeSingleton;

    }

    public boolean sendSMSRecovery(String clientPhone, String code) {

        return sendSMS(clientPhone, code, RECOVERY_MESSAGE);
    }

    public boolean sendSMSCode(String clientPhone, String code) {

        return sendSMS(clientPhone, code, ACCOUNT_MESSAGE);
    }

    private  boolean sendSMS(String clientPhone, String code, String comentario) {
        boolean response;

        try {
            Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

            Message message = Message
                    .creator(
                            new PhoneNumber(ACCOUNT_AREA + clientPhone), // to
                            new PhoneNumber(ACCOUNT_PHONE), // from
                            comentario + code)
                    .create();

            log.info("Sened Message Success --> SID : " + message.getSid());

            response = true;
        }catch (Exception e){
            log.error("Error envio mensaje SMS", e);
            response = false;
        }

        return response;
    }

    /**
     *
     * https://www.twilio.com/try-twilio
     *
     * user : cmallea@gmail.com
     * pass : yT!84!q9K5ZKrs}
     *
     * phone : +14704227976
     * l4GPbnsPwd_dX8DBnCOhh8m-cEznwTWloHsVo3dO
     *
     *
     *
     *
     */



}
