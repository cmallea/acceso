package cl.unab.acceso.login.controlador.template;

import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

abstract public class PageControladorBase {

    public abstract ModelAndView create(Model model, HttpSession httpSession);

    public abstract ModelAndView update(Model model, HttpSession httpSession) ;

    public abstract ModelAndView delete(Model model, HttpSession httpSession) ;

    public abstract ModelAndView list(Model model, HttpSession httpSession) ;

    public abstract ModelAndView listAll(Model model, HttpSession httpSession) ;

}
