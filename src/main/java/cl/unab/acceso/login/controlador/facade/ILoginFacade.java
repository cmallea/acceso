package cl.unab.acceso.login.controlador.facade;

import cl.unab.acceso.login.controlador.singleton.DatoEncriptacionSingleton;
import cl.unab.acceso.login.controlador.singleton.EnvioMensajeSingleton;
import cl.unab.acceso.login.modelo.dto.UsuarioDto;
import cl.unab.acceso.login.vista.UsuarioVo;

import java.util.Random;

public interface ILoginFacade {

    public boolean validLogin(String username, String password, StringBuffer message);

    public boolean validCode(String username, Long code, StringBuffer message);

    public boolean validRecupera(String username, String movil, StringBuffer message);
}
