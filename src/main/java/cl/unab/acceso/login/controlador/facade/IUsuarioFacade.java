package cl.unab.acceso.login.controlador.facade;

import cl.unab.acceso.login.vista.UsuarioVo;

import java.util.List;

public interface IUsuarioFacade {

    public boolean create(UsuarioVo usuarioVo, StringBuffer message);
    public boolean update(UsuarioVo usuarioVo, StringBuffer message);
    public boolean delete(UsuarioVo usuarioVo, StringBuffer message);
    public List<UsuarioVo> listAll(UsuarioVo usuarioVo, StringBuffer message);
    public UsuarioVo list(UsuarioVo usuarioVo, StringBuffer message);
}
