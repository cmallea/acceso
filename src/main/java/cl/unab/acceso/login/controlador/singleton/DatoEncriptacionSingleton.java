package cl.unab.acceso.login.controlador.singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

public class DatoEncriptacionSingleton {

    private static Logger log = LoggerFactory.getLogger(DatoEncriptacionSingleton.class);

    private SecretKeySpec secretKey;
    private byte[] key;

    private static DatoEncriptacionSingleton datoEncriptacionSingleton;

    public synchronized static DatoEncriptacionSingleton getEnvioMensajeSingleton(){
        if(datoEncriptacionSingleton == null){
            datoEncriptacionSingleton = new DatoEncriptacionSingleton();
        }
        return datoEncriptacionSingleton;

    }

    public void setKey(String keyData){
        MessageDigest sha = null;
        try {
            key = keyData.getBytes("UTF-8");
            sha = MessageDigest.getInstance("SHA-1");
            key = sha.digest(key);
            key = Arrays.copyOf(key, 16);
            secretKey = new SecretKeySpec(key, "AES");
        }catch (NoSuchAlgorithmException e) {
            log.error("Error metodo setKey" , e);
        }catch (UnsupportedEncodingException e) {
            log.error("Error metodo setKey" , e);
        }
    }

    public String encrypt(String strToEncrypt, String secret) {

        try {

            this.setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));

        }catch (Exception e){
            log.error("Error metodo encrypt" , e);
        }

        return null;
    }

    public String decrypt(String strToDecrypt, String secret) {
        try {

            this.setKey(secret);
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));

        } catch (Exception e){
            log.error("Error metodo decrypt" , e);
        }

        return null;
    }

    public static void main(String[] args) {

        /*DatoEncriptacionSingleton configEncriptacionUtil= DatoEncriptacionSingleton.getEnvioMensajeSingleton();

        final String secretKey = "password";

        String originalString = "654321";
        String encryptedString = configEncriptacionUtil.encrypt(originalString, secretKey) ;
        String decryptedString = configEncriptacionUtil.decrypt(encryptedString, secretKey) ;

        log.info("Original:["+originalString+"]");
        log.info("Encrypted:["+encryptedString+"]");
        log.info("Decrypted:["+decryptedString+"]");*/
    }
}
