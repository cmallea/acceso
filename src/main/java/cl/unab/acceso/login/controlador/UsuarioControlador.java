package cl.unab.acceso.login.controlador;

import cl.unab.acceso.login.controlador.facade.UsuarioFacade;
import cl.unab.acceso.login.controlador.template.PageControladorBase;
import cl.unab.acceso.login.vista.UsuarioVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class UsuarioControlador extends PageControladorBase {

    private static Logger log = LoggerFactory.getLogger(UsuarioControlador.class);

    @Autowired
    private UsuarioFacade usuarioFacade;

    private UsuarioVo usuarioVo = new UsuarioVo();
    private ModelAndView modelAndView = new ModelAndView("");


    @RequestMapping(value="/create", method= RequestMethod.GET)
    public ModelAndView create(Model model, HttpSession httpSession) {
        log.info("create get");
        StringBuffer message = new StringBuffer();
        usuarioFacade.create(usuarioVo, message);
        modelAndView.addObject("usuarioVista", usuarioVo);
        return modelAndView;
    }

    @RequestMapping(value="/update", method= RequestMethod.GET)
    public ModelAndView update(Model model, HttpSession httpSession) {
        log.info("update get");
        StringBuffer message = new StringBuffer();
        usuarioFacade.update(usuarioVo, message);
        modelAndView.addObject("usuarioVista", usuarioVo);
        return modelAndView;
    }

    @RequestMapping(value="/delete", method= RequestMethod.GET)
    public ModelAndView delete(Model model, HttpSession httpSession) {
        log.info("delete get");
        StringBuffer message = new StringBuffer();
        usuarioFacade.delete(usuarioVo, message);
        modelAndView.addObject("usuarioVista", usuarioVo);
        return modelAndView;
    }

    @RequestMapping(value="/list", method= RequestMethod.GET)
    public ModelAndView list(Model model, HttpSession httpSession) {
        log.info("list get");
        StringBuffer message = new StringBuffer();
        usuarioFacade.list(usuarioVo, message);
        modelAndView.addObject("usuarioVista", usuarioVo);
        return modelAndView;
    }

    @RequestMapping(value="/listAll", method= RequestMethod.GET)
    public ModelAndView listAll(Model model, HttpSession httpSession) {
        log.info("listAll get");
        StringBuffer message = new StringBuffer();
        usuarioFacade.listAll(usuarioVo, message);
        modelAndView.addObject("usuarioVista", usuarioVo);
        return modelAndView;
    }


}
