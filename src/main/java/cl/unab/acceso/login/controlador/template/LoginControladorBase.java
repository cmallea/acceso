package cl.unab.acceso.login.controlador.template;

import cl.unab.acceso.login.vista.UsuarioVo;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

abstract public class LoginControladorBase {

    public abstract ModelAndView root(Model model)  ;

    public abstract ModelAndView login(Model model ) ;

    public abstract ModelAndView code(@ModelAttribute("usuarioVista") UsuarioVo usuarioVO, Model model, HttpSession session) ;

    public abstract ModelAndView login(@ModelAttribute("usuarioVista") UsuarioVo usuarioVista, Model model, HttpSession session) ;

    public abstract ResponseEntity<ModelMap> recupera(@RequestBody ModelMap modelMap) ;

    public abstract ModelAndView logout(Model model, HttpSession httpSession) ;
}
