package cl.unab.acceso.login.controlador.facade;

import cl.unab.acceso.login.controlador.LoginControlador;
import cl.unab.acceso.login.controlador.singleton.DatoEncriptacionSingleton;
import cl.unab.acceso.login.controlador.singleton.EnvioMensajeSingleton;
import cl.unab.acceso.login.modelo.dao.UsuarioDao;
import cl.unab.acceso.login.modelo.dto.UsuarioDto;
import cl.unab.acceso.login.vista.UsuarioVo;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class LoginFacade implements ILoginFacade {

    private static Logger log = LoggerFactory.getLogger(LoginControlador.class);

    private UsuarioDao usuarioDao;

    @Autowired
    public LoginFacade(UsuarioDao usuarioDao){
        this.usuarioDao = usuarioDao;
    }

    private UsuarioVo findUsuario(String username){
        log.info("Metodo Facade: findUsuario ");
        UsuarioVo usuarioVo = null;

        UsuarioDto usuarioDto = usuarioDao.findbyUsername(username);

        if(usuarioDto != null ){
            usuarioVo = new UsuarioVo();
            usuarioVo.setApellidos(usuarioDto.getApellidos());
            usuarioVo.setEmail(usuarioDto.getEmail());
            usuarioVo.setId(usuarioDto.getId());
            usuarioVo.setMovil(usuarioDto.getMovil());
            usuarioVo.setEmail(usuarioDto.getEmail());
            usuarioVo.setRut(usuarioDto.getRut());
            usuarioVo.setNombres(usuarioDto.getNombres());
            usuarioVo.setUsername(usuarioDto.getUsername());
            usuarioVo.setPassword(usuarioDto.getPassword());
            usuarioVo.setCode(usuarioDto.getCode());
        }

        return usuarioVo;
    }

    public boolean validLogin(String username, String password, StringBuffer message){
        log.info("Metodo Facade: validLogin ");
        UsuarioVo usuarioVo = this.findUsuario(username);

        if(usuarioVo != null ){
            if(DatoEncriptacionSingleton
                    .getEnvioMensajeSingleton()
                    .encrypt(password,"password")
                    .equals(usuarioVo.getPassword())){

                Random random = new Random();
                String code = RandomStringUtils.random(5, false, true);

                //String code = "12345"; // TODO : Eliminar para parsar a version final

                String telefono = usuarioVo.getMovil().toString();
                if(EnvioMensajeSingleton.getEnvioMensajeSingleton().sendSMSCode(telefono, code)){
                    usuarioDao.updateUsernameCode(username, code);
                    return true;

                }else{
                    message.append("Problemas en enviar mensaje SMS para validar su cuenta. Intente mas tarde.");
                }

            }else{
                message.append("Usuario o contraseña son incorrectos.");
            }
        }else{
            message.append("Usuario no existe registrado.");
        }

        return false;
    }

    public boolean validCode(String username, Long code, StringBuffer message){
        UsuarioVo usuarioVo = this.findUsuario(username);

        if(usuarioVo != null) {
            if (usuarioVo.getCode().equals(code)) {
                return true;
            }else{
                message.append("Codigo de validacion ingresado es incorrecto, intente nuevamente.");
            }
        }else{
            message.append("Usuario no existe registrado.");
        }

        return false;
    }

    public boolean validRecupera(String username, String movil, StringBuffer message){
        UsuarioVo usuarioVo = this.findUsuario(username);

        if(usuarioVo != null) {
            String movilOrigen = usuarioVo.getMovil().toString();
            movilOrigen = removeCodigoInternacional(movilOrigen);
            movil = removeCodigoInternacional(movil);

            if (movilOrigen.equals(movil)) {

                String code = DatoEncriptacionSingleton
                                .getEnvioMensajeSingleton()
                                .decrypt( usuarioVo.getPassword(),"password");

                if (EnvioMensajeSingleton
                        .getEnvioMensajeSingleton()
                        .sendSMSRecovery(movilOrigen, code)) {
                    return true;
                }else{
                    message.append("Problemas en enviar mensaje SMS para validar su cuenta. Intente mas tarde.");
                }
            }else{
                message.append("Movil no pertenece al usuario que solicita recuperacion de clave, favor intente nuevamente..");
            }
        }else{
            message.append("Usuario no existe registrado.");
        }

        return false;
    }

    private String removeCodigoInternacional(String movil) {
        return movil.replaceAll("\\+56", "");
    }

}
