package cl.unab.acceso.login.controlador;

import cl.unab.acceso.login.controlador.facade.ILoginFacade;
import cl.unab.acceso.login.controlador.facade.LoginFacade;
import cl.unab.acceso.login.config.AppFilterSecurity;
import cl.unab.acceso.login.controlador.template.LoginControladorBase;
import cl.unab.acceso.login.vista.UsuarioVo;
import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class LoginControlador extends LoginControladorBase {

    private static Logger log = LoggerFactory.getLogger(LoginControlador.class);

    @Autowired
    private ILoginFacade loginFacade;

    private UsuarioVo usuarioVo = new UsuarioVo();
    private ModelAndView modelAndView = new ModelAndView("");


    /**
     *
     * @param model
     * @return
     */
    @RequestMapping(value="/", method= RequestMethod.GET)
    public ModelAndView root(Model model) {
        log.info("root get");
        modelAndView.setViewName("acceso/login");
        modelAndView.addObject("usuarioVista", usuarioVo);
        return modelAndView;
    }

    /**
     *
     * @param model
     * @return
     */
    @RequestMapping(value="/login", method= RequestMethod.GET)
    public ModelAndView login(Model model ) {
        log.info("login get");
        modelAndView.addObject("usuarioVista", usuarioVo);
        return modelAndView;
    }

    /**
     *
     * @param usuarioVO
     * @param model
     * @return
     */
    @Before( @BeforeElement(AppFilterSecurity.class))
    @RequestMapping(value="/code", method= RequestMethod.POST)
    public ModelAndView code(@ModelAttribute("usuarioVista") UsuarioVo usuarioVO, Model model, HttpSession session) {
        log.info("codeIndex post");

        String username = (String) session.getAttribute("username");

        StringBuffer message = new StringBuffer();

        if(loginFacade.validCode(username, usuarioVO.getCode(), message)) {
            modelAndView.addObject("usuarioVista", usuarioVO);
            modelAndView.setViewName("principal/index");
            session.setAttribute("code", true);
        }else{
            session.setAttribute("code", false);
            session.setAttribute("error", true);
            session.setAttribute("message", message.toString());
        }


        return modelAndView;
    }

    /**
     *
     * @param usuarioVista
     * @param model
     * @param session
     * @return
     */
    @RequestMapping(value="/login", method= RequestMethod.POST)
    public ModelAndView login(@ModelAttribute("usuarioVista") UsuarioVo usuarioVista, Model model, HttpSession session) {
        log.info("login post");

        StringBuffer message = new StringBuffer();

        modelAndView.addObject("usuarioVista", usuarioVista);

        if(loginFacade.validLogin(usuarioVista.getUsername(), usuarioVista.getPassword(), message)){
            session.setAttribute("auth", true);
            session.setAttribute("error", false);
            session.setAttribute("message", "");
            session.setAttribute("username", usuarioVista.getUsername());

            modelAndView.setViewName("acceso/code");
            return modelAndView;

        }else{
            session.setAttribute("auth", false);
            session.setAttribute("error", true);
            session.setAttribute("message", message.toString());

            model.addAttribute("usuarioVista", usuarioVista);

            modelAndView.setViewName("acceso/login");
            return modelAndView;
        }

    }

    @RequestMapping(value = "/recupera", method = RequestMethod.POST)
    public ResponseEntity<ModelMap> recupera(@RequestBody ModelMap modelMap) {
        log.info("recupera post");

        log.info("usuario : "+ modelMap.get("usuario"));
        String usuario = (String) modelMap.get("usuario");
        log.info("movil : "+ modelMap.get("movil"));
        String movil = (String) modelMap.get("movil");

        StringBuffer message = new StringBuffer();

        if(loginFacade.validRecupera(usuario, movil, message)){
            modelMap.put("code", "0");
            modelMap.put("message", "Envio password por SMS satisfactoriamente.");
        }else{
            modelMap.put("code", "2");
            modelMap.put("message", message.toString());
        }

        return new ResponseEntity<ModelMap>(modelMap, HttpStatus.OK);

    }

    /**
     *
     * @param httpSession
     * @return
     */
    @RequestMapping(value="/logout", method= RequestMethod.GET)
    public ModelAndView logout(Model model, HttpSession httpSession) {
        log.info("logout get");
        httpSession.invalidate();

        modelAndView.setViewName("acceso/login");
        usuarioVo = new UsuarioVo();

        modelAndView.addObject("usuarioVista", usuarioVo);
        modelAndView.addObject("logout", "success");

        return modelAndView;
    }

}
