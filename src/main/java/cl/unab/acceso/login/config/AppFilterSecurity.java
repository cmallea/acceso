package cl.unab.acceso.login.config;

import cl.unab.acceso.login.controlador.LoginControlador;
import com.kastkode.springsandwich.filter.api.AfterHandler;
import com.kastkode.springsandwich.filter.api.BeforeHandler;
import com.kastkode.springsandwich.filter.api.Flow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AppFilterSecurity implements BeforeHandler, AfterHandler {

    private static Logger log = LoggerFactory.getLogger(LoginControlador.class);

    /**
     *
     * @param httpServletRequest
     * @param httpServletResponse
     * @param handlerMethod
     * @param strings
     * @return
     * @throws Exception
     */
    @Override
    public Flow handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, HandlerMethod handlerMethod, String[] strings) throws Exception {

        if(httpServletRequest.getSession().getAttribute("auth") != null){
            boolean auth = (Boolean) httpServletRequest.getSession().getAttribute("auth");
            if(auth){
                log.info("BeforeHandler Authentication true");
            }else{
                httpServletResponse.sendRedirect("/acceso/logout");
                log.info("BeforeHandler Authentication false");
            }
        }else {
            httpServletResponse.sendRedirect("/acceso/logout");
            log.info("BeforeHandler Authentication null");
        }

        return Flow.CONTINUE;
    }

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, HandlerMethod handlerMethod, ModelAndView modelAndView, String[] strings) throws Exception {

        if(httpServletRequest.getSession().getAttribute("auth") != null){
            boolean auth = (Boolean) httpServletRequest.getSession().getAttribute("auth");
            if(auth){
                log.info("AfterHandler Authentication true");
            }else{
                httpServletResponse.sendRedirect("/acceso");
                log.info("AfterHandler Authentication false");
            }
        }else {
            httpServletResponse.sendRedirect("/acceso");
            log.info("AfterHandler Authentication null");
        }
    }
}
