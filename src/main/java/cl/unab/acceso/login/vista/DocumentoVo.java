package cl.unab.acceso.login.vista;

import java.util.List;

public class DocumentoVo {

    private Long id;
    private String comentario;
    private String nombre;
    private Long notaParcial;
    private List<ComentarioVo> comentarios;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getNotaParcial() {
        return notaParcial;
    }

    public void setNotaParcial(Long notaParcial) {
        this.notaParcial = notaParcial;
    }

    public List<ComentarioVo> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<ComentarioVo> comentarios) {
        this.comentarios = comentarios;
    }

}