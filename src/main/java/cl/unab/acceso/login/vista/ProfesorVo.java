package cl.unab.acceso.login.vista;

public class ProfesorVo extends UsuarioVo{

    private String titulo;
    private String espeialidad;

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the espeialidad
     */
    public String getEspeialidad() {
        return espeialidad;
    }

    /**
     * @param espeialidad the espeialidad to set
     */
    public void setEspeialidad(String espeialidad) {
        this.espeialidad = espeialidad;
    }
    
    
    
}