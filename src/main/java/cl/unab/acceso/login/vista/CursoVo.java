package cl.unab.acceso.login.vista;

import java.util.List;

public class CursoVo {

    private Long id;
    private String descripcion;
    private String nombre;
    private List<AlumnoVo> alumnos;
    private ProfesorVo profesor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<AlumnoVo> getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(List<AlumnoVo> alumnos) {
        this.alumnos = alumnos;
    }

    public ProfesorVo getProfesor() {
        return profesor;
    }

    public void setProfesor(ProfesorVo profesor) {
        this.profesor = profesor;
    }
    
    
}