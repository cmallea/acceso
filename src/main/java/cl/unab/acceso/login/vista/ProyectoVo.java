package cl.unab.acceso.login.vista;

import java.util.List;


public class ProyectoVo {

    private Long id;
    private String descripcion;
    private String nombre;
    private CursoVo curso;
    private List<DocumentoVo> documentos;
    private List<AlumnoVo> integrantes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public CursoVo getCurso() {
        return curso;
    }

    public void setCurso(CursoVo curso) {
        this.curso = curso;
    }

    public List<DocumentoVo> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<DocumentoVo> documentos) {
        this.documentos = documentos;
    }

    public List<AlumnoVo> getIntegrantes() {
        return integrantes;
    }

    public void setIntegrantes(List<AlumnoVo> integrantes) {
        this.integrantes = integrantes;
    }

}