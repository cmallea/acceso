package cl.unab.acceso.login.vista;

public class EvaluacionVo {

    private Long id;
    private Long nota;
    private ProyectoVo proyecto;
    private AlumnoVo alumno;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNota() {
        return nota;
    }

    public void setNota(Long nota) {
        this.nota = nota;
    }

    public ProyectoVo getProyecto() {
        return proyecto;
    }

    public AlumnoVo getAlumno() {
        return alumno;
    }

    public void setAlumno(AlumnoVo alumno) {
        this.alumno = alumno;
    }

}