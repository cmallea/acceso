package cl.unab.acceso.login.modelo.dao;

import cl.unab.acceso.login.modelo.dto.UsuarioDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class UsuarioImpl implements UsuarioDao {

    private static Logger log = LoggerFactory.getLogger(UsuarioImpl.class);

    private static NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {

        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public boolean updateUsernameCode(String username, String code) {


        try {
            String select = " UPDATE acceso.usuario " +
                    "SET code=:code " +
                    "WHERE username=:username";

            MapSqlParameterSource paramters = new MapSqlParameterSource();
            paramters.addValue("username", username);
            paramters.addValue("code", code);

            this.jdbcTemplate.update(select, paramters);

            return true;

        }catch (EmptyResultDataAccessException e){
            log.warn("Query  : No existe Registro " + e.getMessage());
        }catch (DataAccessException e){
            e.printStackTrace();
        }

        return false;
    }

    public UsuarioDto findbyUsername(String username){
        try {
            String select = " SELECT id, rut, nombres, apellidos, email, movil, code, username, password " +
                    " FROM acceso.usuario WHERE username=:username ";

            MapSqlParameterSource paramters = new MapSqlParameterSource();
            paramters.addValue("username", username);

            RowMapper<UsuarioDto> rowMapper = new BeanPropertyRowMapper(UsuarioDto.class);

            return this.jdbcTemplate.queryForObject(select, paramters, rowMapper);

        }catch (EmptyResultDataAccessException e){
            log.warn("Query  : No existe Registro " + e.getMessage());
        }catch (DataAccessException e){
            e.printStackTrace();
        }

        return null;
    }

    @Transactional(readOnly=true)
    public UsuarioDto findbyId(Integer id) {

        try {
            String select = " SELECT id, rut, nombres, apellidos, email, movil, code, username, password " +
                            " FROM acceso.usuario WHERE id=:id ";

            MapSqlParameterSource paramters = new MapSqlParameterSource();
            paramters.addValue("id", id);

            RowMapper<UsuarioDto> rowMapper = new BeanPropertyRowMapper(UsuarioDto.class);

            return this.jdbcTemplate.queryForObject(select, paramters, rowMapper);

        }catch (EmptyResultDataAccessException e){
            log.warn("Query  : No existe Registro " + e.getMessage());
        }catch (DataAccessException e){
            e.printStackTrace();
        }

        return null;
    }


    @Transactional(readOnly=true)
    public List<UsuarioDto> findbyAll() {

        try {

            String select = " SELECT id, rut, nombres, apellidos, email, movil, code, username, password " +
                            " FROM acceso.usuario ";

            MapSqlParameterSource paramters = new MapSqlParameterSource();
            return this.jdbcTemplate.query(select, paramters, new BeanPropertyRowMapper(UsuarioDto.class));

        }catch (EmptyResultDataAccessException e){
            log.warn("Query  : No existe Registro " + e.getMessage());
        }catch (DataAccessException e){
            e.printStackTrace();
        }

        return null;


    }



}