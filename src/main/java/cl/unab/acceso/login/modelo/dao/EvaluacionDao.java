package cl.unab.acceso.login.modelo.dao;

import cl.unab.acceso.login.modelo.dto.EvaluacionDto;

import java.util.List;

public interface EvaluacionDao {

    public EvaluacionDto findbyId(Integer id);
    public List<EvaluacionDto> findbyAll();

}
