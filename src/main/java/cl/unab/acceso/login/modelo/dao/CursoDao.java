package cl.unab.acceso.login.modelo.dao;

import cl.unab.acceso.login.modelo.dto.CursoDto;

import java.util.List;

public interface CursoDao {

    public CursoDto findbyId(Integer id);
    public List<CursoDto> findbyAll();

}
