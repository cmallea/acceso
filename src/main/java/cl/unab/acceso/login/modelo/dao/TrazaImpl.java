package cl.unab.acceso.login.modelo.dao;

import cl.unab.acceso.login.modelo.dto.TrazaDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class TrazaImpl implements TrazaDao {

    private static Logger log = LoggerFactory.getLogger(TrazaImpl.class);

    private static NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {

        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    @Transactional(readOnly=true)
    public TrazaDto findbyId(Integer id) {

        try {
            String select = " SELECT id, descripcion, id_usuario " +
                            " FROM acceso.traza WHERE id=:id ";

            MapSqlParameterSource paramters = new MapSqlParameterSource();
            paramters.addValue("id", id);

            RowMapper<TrazaDto> rowMapper = new BeanPropertyRowMapper(TrazaDto.class);

            return this.jdbcTemplate.queryForObject(select, paramters, rowMapper);

        }catch (EmptyResultDataAccessException e){
            log.warn("Query  : No existe Registro " + e.getMessage());
        }catch (DataAccessException e){
            e.printStackTrace();
        }

        return null;
    }


    @Transactional(readOnly=true)
    public List<TrazaDto> findbyAll() {

        try {

            String select = " SELECT id, descripcion, id_usuario " +
                            " FROM acceso.traza ";

            MapSqlParameterSource paramters = new MapSqlParameterSource();
            return this.jdbcTemplate.query(select, paramters, new BeanPropertyRowMapper(TrazaDto.class));

        }catch (EmptyResultDataAccessException e){
            log.warn("Query  : No existe Registro " + e.getMessage());
        }catch (DataAccessException e){
            e.printStackTrace();
        }

        return null;


    }



}