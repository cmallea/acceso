package cl.unab.acceso.login.modelo.dao;

import cl.unab.acceso.login.modelo.dto.DocumentoDto;

import java.util.List;

public interface DocumentoDao {

    public DocumentoDto findbyId(Integer id);
    public List<DocumentoDto> findbyAll();

}
