package cl.unab.acceso.login.modelo.dao;

import cl.unab.acceso.login.modelo.dto.UsuarioDto;

import java.util.List;

public interface UsuarioDao {

    public boolean updateUsernameCode(String username, String code);
    public UsuarioDto findbyUsername(String username);
    public UsuarioDto findbyId(Integer id);
    public List<UsuarioDto> findbyAll();

}
