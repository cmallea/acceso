package cl.unab.acceso.login.modelo.dto;

import cl.unab.acceso.login.vista.UsuarioVo;

public class AlumnoDto extends UsuarioVo {

    private String carrera;
    private String seccion;
    private boolean diurno;

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    /**
     * @return the diurno
     */
    public boolean isDiurno() {
        return diurno;
    }

    /**
     * @param diurno the diurno to set
     */
    public void setDiurno(boolean diurno) {
        this.diurno = diurno;
    }
    
    
    
    
    

}