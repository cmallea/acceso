package cl.unab.acceso.login.modelo.dao;

import cl.unab.acceso.login.modelo.dto.PerfilDto;

import java.util.List;

public interface PerfilDao {

    public PerfilDto findbyId(Integer id);
    public List<PerfilDto> findbyAll();

}
