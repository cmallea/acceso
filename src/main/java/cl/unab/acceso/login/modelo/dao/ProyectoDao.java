package cl.unab.acceso.login.modelo.dao;

import cl.unab.acceso.login.modelo.dto.ProyectoDto;

import java.util.List;

public interface ProyectoDao {

    public ProyectoDto findbyId(Integer id);
    public List<ProyectoDto> findbyAll();

}
