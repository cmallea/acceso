package cl.unab.acceso.login.modelo.dao;

import cl.unab.acceso.login.modelo.dto.ComentarioDto;

import java.util.List;

public interface ComentarioDao {

    public ComentarioDto findbyId(Integer id);
    public List<ComentarioDto> findbyAll();

}
