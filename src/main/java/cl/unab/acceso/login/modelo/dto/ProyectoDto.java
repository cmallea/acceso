package cl.unab.acceso.login.modelo.dto;

import cl.unab.acceso.login.vista.AlumnoVo;
import cl.unab.acceso.login.vista.CursoVo;
import cl.unab.acceso.login.vista.DocumentoVo;

import java.util.List;


public class ProyectoDto {

    private Long id;
    private String descripcion;
    private String nombre;
    private cl.unab.acceso.login.vista.CursoVo curso;
    private List<cl.unab.acceso.login.vista.DocumentoVo> documentos;
    private List<cl.unab.acceso.login.vista.AlumnoVo> integrantes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public cl.unab.acceso.login.vista.CursoVo getCurso() {
        return curso;
    }

    public void setCurso(CursoVo curso) {
        this.curso = curso;
    }

    public List<cl.unab.acceso.login.vista.DocumentoVo> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<DocumentoVo> documentos) {
        this.documentos = documentos;
    }

    public List<cl.unab.acceso.login.vista.AlumnoVo> getIntegrantes() {
        return integrantes;
    }

    public void setIntegrantes(List<AlumnoVo> integrantes) {
        this.integrantes = integrantes;
    }

}