package cl.unab.acceso.login.modelo.dto;

import cl.unab.acceso.login.vista.ComentarioVo;

import java.util.List;

public class DocumentoDto {

    private Long id;
    private String comentario;
    private String nombre;
    private Long notaParcial;
    private List<cl.unab.acceso.login.vista.ComentarioVo> comentarios;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getNotaParcial() {
        return notaParcial;
    }

    public void setNotaParcial(Long notaParcial) {
        this.notaParcial = notaParcial;
    }

    public List<cl.unab.acceso.login.vista.ComentarioVo> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<ComentarioVo> comentarios) {
        this.comentarios = comentarios;
    }

}