package cl.unab.acceso.login.modelo.dao;

import cl.unab.acceso.login.modelo.dto.CursoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class CursoImpl implements CursoDao {

    private static Logger log = LoggerFactory.getLogger(CursoImpl.class);

    private static NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {

        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    @Transactional(readOnly=true)
    public CursoDto findbyId(Integer id) {

        try {
            String select = " SELECT id, nombre, descripcion " +
                            " FROM acceso.curso WHERE id=:id ";

            MapSqlParameterSource paramters = new MapSqlParameterSource();
            paramters.addValue("id", id);

            RowMapper<CursoDto> rowMapper = new BeanPropertyRowMapper(CursoDto.class);

            return this.jdbcTemplate.queryForObject(select, paramters, rowMapper);

        }catch (EmptyResultDataAccessException e){
            log.warn("Query  : No existe Registro " + e.getMessage());
        }catch (DataAccessException e){
            e.printStackTrace();
        }

        return null;
    }


    @Transactional(readOnly=true)
    public List<CursoDto> findbyAll() {

        try {

            String select = " SELECT id, nombre, descripcion " +
                            " FROM acceso.curso ";

            MapSqlParameterSource paramters = new MapSqlParameterSource();
            return this.jdbcTemplate.query(select, paramters, new BeanPropertyRowMapper(CursoDto.class));

        }catch (EmptyResultDataAccessException e){
            log.warn("Query  : No existe Registro " + e.getMessage());
        }catch (DataAccessException e){
            e.printStackTrace();
        }

        return null;


    }



}