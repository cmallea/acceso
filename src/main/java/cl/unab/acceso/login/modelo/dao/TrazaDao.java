package cl.unab.acceso.login.modelo.dao;

import cl.unab.acceso.login.modelo.dto.TrazaDto;

import java.util.List;

public interface TrazaDao {

    public TrazaDto findbyId(Integer id);
    public List<TrazaDto> findbyAll();

}
