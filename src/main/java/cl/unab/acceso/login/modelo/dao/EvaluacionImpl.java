package cl.unab.acceso.login.modelo.dao;

import cl.unab.acceso.login.modelo.dto.EvaluacionDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class EvaluacionImpl implements EvaluacionDao {

    private static Logger log = LoggerFactory.getLogger(EvaluacionImpl.class);

    private static NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {

        this.jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
    @Transactional(readOnly=true)
    public EvaluacionDto findbyId(Integer id) {

        try {
            String select = " SELECT id, id_proyecto, id_usuario, nota " +
                            " FROM acceso.evaluacion WHERE id=:id ";

            MapSqlParameterSource paramters = new MapSqlParameterSource();
            paramters.addValue("id", id);

            RowMapper<EvaluacionDto> rowMapper = new BeanPropertyRowMapper(EvaluacionDto.class);

            return this.jdbcTemplate.queryForObject(select, paramters, rowMapper);

        }catch (EmptyResultDataAccessException e){
            log.warn("Query  : No existe Registro " + e.getMessage());
        }catch (DataAccessException e){
            e.printStackTrace();
        }

        return null;
    }


    @Transactional(readOnly=true)
    public List<EvaluacionDto> findbyAll() {

        try {

            String select = " SELECT id, id_proyecto, id_usuario, nota " +
                            " FROM acceso.evaluacion ";

            MapSqlParameterSource paramters = new MapSqlParameterSource();
            return this.jdbcTemplate.query(select, paramters, new BeanPropertyRowMapper(EvaluacionDto.class));

        }catch (EmptyResultDataAccessException e){
            log.warn("Query  : No existe Registro " + e.getMessage());
        }catch (DataAccessException e){
            e.printStackTrace();
        }

        return null;


    }



}