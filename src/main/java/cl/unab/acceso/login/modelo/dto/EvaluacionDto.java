package cl.unab.acceso.login.modelo.dto;

import cl.unab.acceso.login.vista.AlumnoVo;
import cl.unab.acceso.login.vista.ProyectoVo;

public class EvaluacionDto {

    private Long id;
    private Long nota;
    private ProyectoVo proyecto;
    private cl.unab.acceso.login.vista.AlumnoVo alumno;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNota() {
        return nota;
    }

    public void setNota(Long nota) {
        this.nota = nota;
    }

    public ProyectoVo getProyecto() {
        return proyecto;
    }

    public cl.unab.acceso.login.vista.AlumnoVo getAlumno() {
        return alumno;
    }

    public void setAlumno(AlumnoVo alumno) {
        this.alumno = alumno;
    }

}