package cl.unab.acceso.login.modelo.dto;

import cl.unab.acceso.login.vista.PerfilVo;
import cl.unab.acceso.login.vista.TrazaVo;

import java.util.List;

public class UsuarioDto {

    private Long id;
    private String apellidos;
    private Long code;
    private String email;
    private Long movil;
    private String nombres;
    private String password;
    private String rut;
    private String username;
    private List<PerfilVo>  perfiles;
    private List<TrazaVo>  trazas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getMovil() {
        return movil;
    }

    public void setMovil(Long movil) {
        this.movil = movil;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<PerfilVo> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(List<PerfilVo> perfiles) {
        this.perfiles = perfiles;
    }

    public List<TrazaVo> getTrazas() {
        return trazas;
    }

    public void setTrazas(List<TrazaVo> trazas) {
        this.trazas = trazas;
    }
    
    
}