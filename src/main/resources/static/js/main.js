 function recovery(){
    Swal.mixin({
          confirmButtonText: 'Continuar',
          showCancelButton: true,
          progressSteps: ['1', '2']
        }).queue([
          {
            input: 'text',
            inputValidator: (value) => {
                 if (!value) {
                   return 'Valor requerido para continuar';
                 }
                },
            title: 'Paso 1',
            text: 'Ingrese Nombre de Usuario',
            inputPlaceholder: 'Ej : Anonymous'

          },
          {
           input: 'number',
           inputValidator: (value) => {
             if (!value) {
               return 'Valor requerido para continuar';
             }
            },
            title: 'Paso 2',
            text: 'Ingrese su Numero de Movil',
            inputPlaceholder: 'Ej : 948564834'
          }
        ]).then((result) => {
          if (result.value) {
            Swal.fire({
              title: 'Procedimiento de Recuperacion de Acceso',
              text: 'Se enviara a su movil un mensaje SMS con su password de acceso, favor revizar despues de esta accion.',
              showCancelButton: true,
              confirmButtonText: 'Enviar SMS',
              showLoaderOnConfirm: true,
              preConfirm: (answers) => {
              return $.ajax({
                   headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'application/json'
                      },
                     url: 'recupera',
                     type: 'post',
                     data: '{"usuario": "'+result.value[0]+'", "movil": "'+result.value[1]+'"}'
                   })
                   .then(response => {
                     return response
                   })
                   .catch(error => {
                     console.log(error); // Nice to view which properties 'error' has
                      Swal.fire({
                         title: 'Envio Mensaje SMS',
                         icon: 'error',
                         text: 'A ocurrido un error: '+error.status
                       })
                    })
              },
              allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                console.log(result)
                if(result.value.code == '0'){
                      Swal.fire({
                        title: 'Proceso Recuperacion Password',
                        icon: 'success',
                        text: result.value.message
                      })
                }else{
                     Swal.fire({
                        title: 'Proceso Recuperacion Password',
                        icon: 'error',
                        text: result.value.message
                      })
                }
            })

          } // Cierre de then

        }) // Cierre de Mixin
    }